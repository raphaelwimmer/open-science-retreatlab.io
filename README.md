# Welcome to the Open Science Retreat website project!
:tada: :wave: :wink:

## Making edits to the website

In the following I will explain how to make edits to the website using the
terminal.

Before making edits, you need to have rights to create branches in the project. You can also [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the project.

1. Clone the project :arrow_down:
```
git clone git@gitlab.com:open-science-retreat/open-science-retreat.gitlab.io.git
```
or your fork
```
git clone git@gitlab.com:YOURUSERNAME/open-science-retreat.gitlab.io.git
```

2. Enter the project directory
```
cd open-science-retreat.gitlab.io
```

3. Create a branch for your change (replace `my-new-branch` with a useful branch
name) :tanabata_tree:
```
git checkout -b my-new-brach
```

4. Now make the changes in the files you want to change.

5. Optional: install [jekyll](https://jekyllrb.com/docs/) and all dependencies for the website (`bundle install`) and check what the website looks like 
```
bundle exec jekyll serve
```
The output tells you (a) if everything went ok, and (b) how to look at the
website (for me it is `http://127.0.0.1:4000/`, which I can open in any
browser). Go back to step 4 until everything is to your liking. 

6. Commit and push your changes :white_check_mark:. You might need
to stop (`control + c`) jekyll before being able to do this.
In this example, I am assuming you changed the file `_pages/home.md` 
```
git add _pages/home.md
git commit -m "Update home"
git push --set-upstream origin my-new-branch
```  

7. Now go to https://gitlab.com/open-science-retreat/open-science-retreat.gitlab.io/-/merge_requests and create a merge request. If you've created a fork, check [here](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream) for help on merging forks, if needed. Once the merge request is accepted, your changes will go live under https://open-science-retreat.gitlab.io/. :tada:


