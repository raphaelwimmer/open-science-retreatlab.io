---
layout: splash
permalink: /faq/
title: "FAQ"
hidden: true
header:
  overlay_color: "#594777"
  overlay_image: "/assets/images/night.jpg"
excerpt: "Answers to your questions"
---

### Can I bring my loved ones to the retreat?

We are trying to be as inclusive as possible. We hope to make it possible for
you to bring kids/friends/partners/dogs/..., but we'll have to discuss the
specifics with you directly. 
Please [get in touch](https://open-science-retreat.gitlab.io/contact/).

Prices: Your loved ones do not have to pay full price. They pay only for the
stay and meals (board and lodging). Children younger than 2 years are free, 3-9
years 25% of regular price of board and lodging, from 10 years full price of 
board and lodging.


### What will be the COVID safety measures?

We want this event to be both safe and enjoyable for all. Depending on what the
situation will be like in April, we will put safety measures in place.
The details will be discussed with the participants prior to the meeting, but
likely we will at least do a test every morning. 


### I have a disability. Can I join the event?

We are trying to be as inclusive as possible. There are accessible hotel rooms
on site. Depending on your disability you might need different things that we
should discuss. Please [get in touch](https://open-science-retreat.gitlab.io/contact/)
so that we can make the event a perfect experience for you.


### The event looks great, but I can't afford it. What can I do?

We will have as many stipends available as possible. We are currently
looking for sponsors and will send out info on stipends via the 
Open Science Retreat
[mailing list](https://www.getrevue.co/profile/open-science-retreat).


### I'd like to sponsor the event. How do I do that?

We'd love to get sponsoring to make it possible for everyone to join (also
those who cannot afford it). Please [get in
touch](https://open-science-retreat.gitlab.io/contact/).


### Can I arrive on Sunday?

Regular check-in is on Monday. Early arrival on Sunday is possible.  


### What activities are available nearby

You can go hiking, to the [nearby spa](https://goo.gl/maps/2FqThRWLZEzrNbHZ6),
explore the lake and marshlands, or just relax at the site.


### I have a question that was not answered here. What can I do?

Please [get in touch](https://open-science-retreat.gitlab.io/contact/).


---

## Don't miss any updates

Sign up for e-mail updates on the Open Science Retreat:

<div id="revue-embed">
  <form action="https://www.getrevue.co/profile/open-science-retreat/add_subscriber" method="post" id="revue-form" name="revue-form"  target="_blank">
  <div class="revue-form-group">
    <label for="member_email">Email address</label>
    <input class="revue-form-field" placeholder="Your email address..." type="email" name="member[email]" id="member_email">
  </div>
  <div class="revue-form-actions">
    <input type="submit" value="Subscribe" name="member[subscribe]" id="member_submit">
  </div>
  <div class="revue-form-footer">By subscribing, you agree with Revue’s <a target="_blank" href="https://www.getrevue.co/terms">Terms of Service</a> and <a target="_blank" href="https://www.getrevue.co/privacy">Privacy Policy</a>.</div>
  </form>
</div>

[View previous updates](https://www.getrevue.co/profile/open-science-retreat){: .btn .btn--success}
