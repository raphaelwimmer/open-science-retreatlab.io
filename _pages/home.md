---
layout: splash
permalink: /
hidden: true
header:
  overlay_color: "#594777"
  overlay_image: "/assets/images/night.jpg"
  og_image: "/assets/images/lake_social.jpg"
excerpt: "April 3-7, 2023" 
feature_row:
  - image_path: /assets/images/jumping.jpg
    alt: "A woman jumping. Trees surround her."
    title: "A retreat, finally!"
    excerpt: "Have you been waiting for an Open Science event where you can learn, work and reboot?"
    url: "/about/"
    btn_class: "btn--primary"
    btn_label: "About"
  - image_path: /assets/images/april.jpg
    alt: "A notebook with the word April written on it in nice lettering."
    title: "A program that suits your needs."
    excerpt: "Lots of time to think and work, but also to relax and move."
    url: "/program/"
    btn_class: "btn--primary"
    btn_label: "Program"
  - image_path: /assets/images/lake.jpg
    alt: "A lake and mountains."
    title: "A great location"
    excerpt: "With a view of the lake and the alps, this is the perfect venue for a retreat."
    url: "/venue/"
    btn_class: "btn--primary"
    btn_label: "Venue"      
gallerysponsors:
  - url: https://www.fz-juelich.de/en
    image_path: /assets/images/supporters/logo-fzj.jpg
    alt: "Logo FZJ"
  - url: https://www.osc.uni-muenchen.de
    image_path: /assets/images/supporters/logo-osc.jpg
    alt: "Logo LMU Open Science Center"
gallerysupporters:
  - url: https://open-science-freelancers.gitlab.io/
    image_path: /assets/images/supporters/logo-osfreelancers.png
    alt: "Logo Open Science Freelancers"
  - url: https://www.trovbase.com
    image_path: /assets/images/supporters/logo-trovbase.png
    alt: "Logo Trovbase"
  - url: https://access2perspectives.org/
    image_path: /assets/images/supporters/logo-a2p.png
    alt: "Logo Access2Perspectives"
---

{% include feature_row %}



## Don't miss any updates

Sign up for e-mail updates on the Open Science Retreat:

<div id="revue-embed">
  <form action="https://www.getrevue.co/profile/open-science-retreat/add_subscriber" method="post" id="revue-form" name="revue-form"  target="_blank">
  <div class="revue-form-group">
    <label for="member_email">Email address</label>
    <input class="revue-form-field" placeholder="Your email address..." type="email" name="member[email]" id="member_email">
  </div>
  <div class="revue-form-actions">
    <input type="submit" value="Subscribe" name="member[subscribe]" id="member_submit">
  </div>
  <div class="revue-form-footer">By subscribing, you agree with Revue’s <a target="_blank" href="https://www.getrevue.co/terms">Terms of Service</a> and <a target="_blank" href="https://www.getrevue.co/privacy">Privacy Policy</a>.</div>
  </form>
</div>

[View previous updates](https://www.getrevue.co/profile/open-science-retreat){: .btn .btn--success}


## Sponsors

{% include gallery id="gallerysponsors" layout="third" %}


## Supporters

{% include gallery id="gallerysupporters" layout="third" %}


