---
permalink: /program/
layout: splash
header:
  overlay_color: "#594777"
author_profile: false
title: "Program"
---

You can find the program draft here: 
[Draft Program](https://docs.google.com/spreadsheets/d/1_eDORwvv-cahAtrO6aAqgeMH2MV6wiTzxON8olxxkkQ/edit?usp=sharing){: .btn .btn--primary}

This event is 
- part [unconference](/program/#unconference), 
- part [focused work](/program/#time-to-focus), 
- part [rebooting](/program/#time-to-reboot).


![A notebook with the word April written on it](/assets/images/april.jpg){: width="300" .align-center}


## Unconference

As a participant you can shape the unconference program (mornings). How? We
will (mis)use GitLab issues.  You can formulate your ideas and discuss the
ideas of others. On the first day of the event we will decide on which ideas we
want to pursue.

[Shape the program](https://gitlab.com/open-science-retreat/open-science-retreat-2023){: .btn .btn--primary}

**Please only participate in shaping the program once you've signed up for the event.**

The idea for shaping the program this way comes from the [ROpenSci unconferences](https://unconf16.ropensci.org/#schedule). 


## Time to focus

The afternoons are your time. You can focus on your own work, work with others
on things we did in the morning, or take time to ponder/plan (if you want, with
support/coaching).


## Time to reboot

To get you out of your work routine, we have a bunch of activities planned for you.

- Walks
- Jogging
- Yoga
- Reflection time
- Social activities


