---
permalink: /venue/
layout: splash
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/lake.jpg
author_profile: false
title: "Venue"
---

The retreat will be at Aspenstein Castle, a little castle at lake Kochel
(Kochelsee). From there you have a beautiful view of the lake and the alps. It
is the perfect place to reboot.

<figure style="width: 900px" class="align-center">
  <img src="https://www.vollmar-akademie.de/wp-content/uploads/2021/05/gvva-luftbild-1024x577-1.jpg" alt="Aspenstein Castle">
  <figcaption>Image credit: Georg-von-Vollmar-Akademie e.V.</figcaption>
</figure> 

You can go on a [virtual tour](http://wichary.net/panorama/tour/vollmar-akademie/index.html) of the venue. 

#### Address:

Georg-von-Vollmar-Akademie e.V.  
Schloss Aspenstein  
Am Aspensteinbichl 9-11  
82431 Kochel am See

Website (German only): [www.vollmar-akademie.de/tagungsort/](https://www.vollmar-akademie.de/tagungsort/)


#### How to get there:

- By train: Search for "Kochel" at [bahn.com](https://www.bahn.com/en).
- By bike: The area is nice for cycling. April can still be cold in this area so check the weather.
- By car: Here's the locaton on [maps](https://goo.gl/maps/pkDvKrcv3daeBTzJ8).
- By airplane: Probably best to fly into Munich and then take the train from there. 

If you have a long journey or want to enjoy the nice location for an extra
day, consider [arriving on Sunday](/faq/#can-i-arrive-on-sunday).
